const btnCalcular = document.getElementById('btnCalcular');
const resultadoIMC = document.getElementById('IMC');
const nivelIMC = document.getElementById('nivelIMC');
const imagenNivel = document.getElementById('imagenNivel');

btnCalcular.addEventListener("click", function () {
    //obtener los datos de los input
    let peso = parseFloat(document.getElementById('peso').value);
    let altura = parseFloat(document.getElementById('altura').value);
    /* hacer calculos*/
    let alturA = altura * altura;
    let imc = peso / alturA;

    /*mostrar datos*/
    resultadoIMC.value = imc.toFixed(2);

    // Determinar el nivel del IMC y mostrar la imagen correspondiente
    if (imc < 18.5) {
        nivelIMC.textContent = "Peso por debajo de lo normal";
        imagenNivel.src = "/img/01.png";
        imagenNivel.alt = "Peso por debajo de lo normal";
    } else if (imc >= 18.5 && imc < 25) {
        nivelIMC.textContent = "Peso saludable";
        imagenNivel.src = "/img/02.png";
        imagenNivel.alt = "Peso saludable";
    } else if (imc >= 25 && imc < 30) {
        nivelIMC.textContent = "Sobrepeso";
        imagenNivel.src = "/img/03.png";
        imagenNivel.alt = "Sobrepeso";
    } else if (imc >= 30 && imc < 35) {
        nivelIMC.textContent = "Obesidad tipo I";
        imagenNivel.src = "/img/04.png";
        imagenNivel.alt = "Obesidad tipo I";
    } else if (imc >= 35 && imc < 40) {
        nivelIMC.textContent = "Obesidad Tipo II";
        imagenNivel.src = "/img/05.png";
        imagenNivel.alt = "Obesidad Tipo II";
    } else {
        nivelIMC.textContent = "Obesidad Tipo III";
        imagenNivel.src = "/img/06.png";
        imagenNivel.alt = "Obesidad Tipo III";
    }
});