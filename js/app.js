/* declarar variables*/

const btnCalcular=document.getElementById('btnCalcular');
const btnLimpiar=document.getElementById('btnLimpiar');
const aside = document.querySelector('aside');

btnCalcular.addEventListener("click",function(){
//obtener los datos de los input
let valorAuto = parseFloat(document.getElementById('valorAuto').value);
    let pInicial = parseFloat(document.getElementById('porcentaje').value);
    let plazos = parseFloat(document.getElementById('plazos').value);

/* hacer calculos*/
let pagoInicia=valorAuto*(pInicial/100);
let totalFin=valorAuto-pagoInicia;
let pagoMensual=totalFin/plazos;
/*mostrar datos*/
document.getElementById('pagoInicial').value=pagoInicia.toFixed(2);
document.getElementById('totalfin').value=totalFin.toFixed(2);
document.getElementById('pagoMensual').value=pagoMensual.toFixed(2);

// Crear elementos de párrafo para mostrar los datos
let parrafo = document.createElement('p');
parrafo.textContent = `Pago Inicial: $${pagoInicial.toFixed(2)}, Total a Financiar: $${totalFinanciar.toFixed(2)}, Pago Mensual: $${pagoMensual.toFixed(2)}`;

// Agregar el párrafo al aside
aside.appendChild(parrafo);
});

function arribaMouse(){
    parrafo = document.getElementById('pa');
    parrafo.style.color="#FF00FF";
    parrafo.style.fontSize='25px';
    parrafo.style.textAlign = 'justify';
}
function salirMouse(){
    parrafo=document.getElementById('pa');
    parrafo.style.color="red";
    parrafo.style.fontSize='17px';
    parrafo.style.textAlign = 'initial';
}

function limpiar(){
    let parrafo=document.getElementById('pa');
    parrafo.textContent="";
}

btnLimpiar.addEventListener('click', limpiar);